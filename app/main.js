import PIXI from 'pixi.js';
import * as Utils from './utils.js';

let stage;
let blocksArr = [];
let openList = [];
let closedList = [];
let parent;
let endRow = 4;
let endCol = 9;
let startRow = 4;
let currRow = startRow;
let startCol = 3;
let currCol = startCol;
let count = 0;
let finalPath = [];
let totalNodes = 0;
let isSettingStart = false;
let startBlock;
let endBlock;
let destBlock;
let isSettingEnd = false;
let isSettingPath = false;
const yellow = '0xffff00';
const purple = 0xff00ff;
const black = 0x000000;
const red = 0xff0000;
const green = 0x00FF00;
const white = '0xffffff';
const grey = 0x121f1f;
const darkblue = 0x006633;
const blue = 0x000080;
const CANVAS_WIDTH = 600;
const CANVAS_HEIGHT = 600;
export const blockWidth = 30;
export const blockHeight = 30;
export const MAX_COLS = 20;
export const MAX_ROWS = 20;

class Main {

	constructor () {
        this.renderer = PIXI.autoDetectRenderer(CANVAS_WIDTH, CANVAS_HEIGHT, {backgroundColor: 0x1099bb});
        document.getElementById('container').appendChild(this.renderer.view);
        stage = new PIXI.Container();
        document.getElementById('set-start').onclick = ()=> {
          isSettingStart = true;
          isSettingEnd = false;
      };

        document.getElementById('set-path').onclick = ()=> {
          isSettingStart = false;
          isSettingEnd = false;
          isSettingPath = true;
      };

        document.getElementById('set-end').onclick = ()=> {
          isSettingEnd = true;
          isSettingStart = false;
      };

        document.getElementById('trace-path').onclick = ()=> {
          this.clearPath()
          isSettingPath = false;
          isSettingStart = false;
          isSettingEnd = false;
          count = 0;
          openList = [];
          closedList = [];
          openList.push(blocksArr[startRow][startCol]);
          this.calculateMovingDistFromTarget(startRow, startCol, blocksArr[startRow][startCol]);
      };
        this.init();
    }

    init () {
        let xpos = 0, ypos = 0, rows = MAX_ROWS, cols = MAX_COLS, numBlocks = 100;

        for (let i = 0; i < rows; i++) {
            let colsArr = [];
            for (let j = 0; j < cols; j++) {
                let isWalkable;
                let row = i;
                let col = j;

                isWalkable = true;

                let squareContainer = new PIXI.Sprite(),
                square = new PIXI.Graphics();
                squareContainer.addChild(square);
                stage.addChild(squareContainer);
                squareContainer.anchor.x = 0.5;
                squareContainer.anchor.y = 0.5;
                squareContainer.x = xpos;
                squareContainer.y = ypos;
                squareContainer.interactive = true;

                squareContainer.on('click', (e)=>{
                  let block = e.target.blockData;
                  if (isSettingPath) {
                    if (block.isWalkable) {
                      block.isWalkable = false;
                      Utils.colorBlock(block.square.getChildAt(0), red, white);
                    }
                    else {
                      block.isWalkable = true;
                      Utils.colorBlock(block.square.getChildAt(0), black, white);
                    }
                  }

                  if (isSettingStart) {
                    if (startBlock) {
                      Utils.colorBlock(startBlock, black, white);
                    }

                    startBlock = block.square.getChildAt(0);
                    block.isStart = true;
                    Utils.colorBlock(startBlock, yellow, white);

                    startCol = block.col;
                    startRow = block.row;
                  }

                  if (isSettingEnd) {
                    if (endBlock) {
                      Utils.colorBlock(endBlock, black, white);
                    }

                    endBlock = block.square.getChildAt(0);
                    Utils.colorBlock(endBlock, purple, white);
                    block.isEnd = true;
                    endCol = block.col;
                    endRow = block.row;
                    destBlock = blocksArr[endRow][endCol];

                  }

                });

                // if (row >= 0 && row < 8 && col > 5 && col < 8) {
                //     isWalkable = false;
                //     Utils.colorBlock(square, red, white);
                // }

				let blockData = {
					id: PIXI.utils.uid(),//Math.round(Math.random()*10000),
					parent: undefined,
					row: i,
					col: j,
					square: squareContainer,
					isWalkable: isWalkable,
					hCost: 0,
					gCost: 0,
					fCost:0
				};

                squareContainer.blockData = blockData;

				colsArr[j] = blockData;

				let squareRef = colsArr[j].square.getChildAt(0);
				if (colsArr[j].isWalkable) {
					Utils.colorBlock(squareRef, black, white);
				}
				else {
					Utils.colorBlock(squareRef, red, white);
				}
                xpos += blockWidth;
    		}

			blocksArr.push(colsArr);
			xpos = 0;
			ypos += blockWidth;
    	}

        startBlock = blocksArr[startRow][startCol].square.getChildAt(0);
        blocksArr[startRow][startCol].isStart = true;
        Utils.colorBlock(startBlock, yellow, white);

        endBlock = blocksArr[endRow][endCol].square.getChildAt(0);
        blocksArr[endRow][endCol].isEnd = true;
        Utils.colorBlock(endBlock, purple, white);

        destBlock = blocksArr[endRow][endCol];

        openList.push(blocksArr[startRow][startCol]);

        this.animate();

        //this.calculateMovingDistFromTarget(startRow, startCol);
    }

    getGoalDistance (currentNode, goal) {
        let dx = Math.abs(currentNode.row - goal.row);
        let dy = Math.abs(currentNode.col - goal.col);

        return Math.sqrt(dx*dx + dy*dy);
    }

    getManhattanDistance (currentNode, goal) {
        let dx = Math.abs(currentNode.row - goal.row);
        let dy = Math.abs(currentNode.col - goal.col);

        return Math.abs(dx) + Math.abs(dy);
    }

    getDistance (currentNode, adjacentNode) {
        let STRAIGHT_MOVE = 1;
        let DIAGONAL_MOVE = Math.sqrt(2);

        if (adjacentNode.row === currentNode.row || adjacentNode.col === currentNode.col) {
            return STRAIGHT_MOVE;
        }
        else {
            return DIAGONAL_MOVE;
        }
    }

    calculateMovingDistFromTarget (currentRow, currentCol) {

        while(openList.length > 0) {

            let	currentNode = this.getNextMove();

            // if current node reached destination
            if (currentNode.row === endRow && currentNode.col === endCol) {
                this.getPath(currentNode);
                return;
            }

            // remove current node from open list and add it to closed list
            openList.splice(openList.indexOf(currentNode),1);

            closedList.push(currentNode);

            // trace currentNode

            if (currentNode.id !== blocksArr[startRow][startCol].id) {
                Utils.colorBlock(currentNode.square.getChildAt(0), grey, white);
            }

            for (var i = 0; i < 9; i ++) {
                if (i === 4 || i === 0 || i === 2 || i === 6 || i === 8) {
                    continue;
                }
                let xi = (i % 3) - 1;
                let yi = Math.floor(i / 3) - 1; // 0 -1, 1 -1, 2 -1, 3 0, 5 0, 6 1, 7 1, 8 1
                let gCostIsBest = false;

                if (currentNode.row + xi >= 0 && currentNode.row + xi < MAX_ROWS && currentNode.col + yi >= 0 && currentNode.col + yi < MAX_COLS) {
                    let node = blocksArr[currentNode.row + xi][currentNode.col + yi];

                    let gCost = currentNode.gCost + 1;
                    //let gCost = currentNode.gCost + this.getDistance(node, destBlock);

                     let hCost = this.getGoalDistance (node, destBlock);
                    //let hCost = this.getManhattanDistance (node, destBlock);

                    if (Utils.containsBlock(node, closedList) || !node.isWalkable) {
                        continue;
                    }
                    console.log('g', gCost, node.gCost);
                    if (!Utils.containsBlock(node, openList)) {
                        gCostIsBest = true;

                        node.hCost = hCost;

                        openList.push(node);
                    }
                    else if (gCost < node.gCost) {
                        console.log('used node')
                        gCostIsBest = true;
                    }

                    if (gCostIsBest) {
                        node.parent = currentNode;
                        node.gCost = gCost;
                        node.fCost = gCost + hCost;
                    }

                }
                else {
                    continue;
                }
            }

        }
    }

    getNextMove () {
      // get the node with lowest f cost in open list
      let lowIndex = 0;
      let prevNode = closedList[closedList.length-1];
      for(let j = 0; j < openList.length; j ++) {
        let openNode = openList[j];
        openNode.fCost = openNode.gCost + openNode.hCost;

        if (openNode.fCost < openList[lowIndex].fCost) {
            lowIndex = j;
        }
      }
      return openList[lowIndex];
    }

    debugMove (currentNode, isFirst) {
        setTimeout(()=> {
            this.calculateMovingDistFromTarget(currentNode.row, currentNode.col);

            if (!isFirst){
                Utils.colorBlock(currentNode.square.getChildAt(0), darkblue, white);
            }
        },50);
    }

    moveToBlock (currentNode) {

        if (currentNode < totalNodes) {
            setTimeout(()=> {
                Utils.colorBlock(finalPath[currentNode].square.getChildAt(0), blue, white);
                currentNode ++;
                this.moveToBlock(currentNode);
            },50);
        }
    }

    clearPath () {
        for(var i = 0; i < finalPath.length; i++) {
            if (finalPath[i].isWalkable) {
                Utils.colorBlock(finalPath[i].square.getChildAt(0), black, white);
            }
        }
    }

    getPath (currentNode) {
      let node = currentNode;
      finalPath = [];

      while (node.parent !== undefined) {
        finalPath.push(node);
        node = node.parent;
      }

      finalPath.reverse().pop();
      totalNodes = finalPath.length;
      this.moveToBlock(0);
    }

    animate() {
        requestAnimationFrame(this.animate.bind(this));
        this.renderer.render(stage);
    }
}

var main = new Main ();
