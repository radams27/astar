import {blockWidth, blockHeight, MAX_COLS, MAX_ROWS} from './main';

function clearSquare (square) {

  for(let i = 1; i < square.children.length; i++) {
    if(square.children[i].hasOwnProperty('id')) {
       if(square.children[i].id === 'g') {
      //console.log('s',square.children[i]);
       square.removeChild(square.children[i]);
      }

      if(square.children[i].id === 'f') {
         square.removeChild(square.children[i]);
      }
    }
  }
}

function clearGCost (square) {
  for(let i = 1; i < square.children.length; i++) {
    if(square.children[i].hasOwnProperty('id')) {
      if(square.children[i].id === 'newG') {
       square.removeChild(square.children[i]);
      }
    }
  }
}

function colorBlock (block, color, lineStyle) {
  block.beginFill(color, 1);
  //block.lineStyle(1, lineStyle, 1);
  block.drawRect(0, 0, blockWidth, blockHeight);
  block.endFill();
}

function isOutOfBounds (currentRow, currentCol) {
  if (currentCol >= MAX_COLS || currentRow >= MAX_ROWS || currentCol < 0 || currentRow < 0) {
    return true;
  }
  return false;
}

function containsBlock (block, list) {
  for(let i = 0; i < list.length; i++) {
    if(block.id === list[i].id) {
      return list[i];
    }
  }
  return false;
}

module.exports = {
    clearSquare: clearSquare,
    clearGCost: clearGCost,
    colorBlock: colorBlock,
    isOutOfBounds: isOutOfBounds,
    containsBlock: containsBlock
}
